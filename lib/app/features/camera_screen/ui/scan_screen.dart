import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_mrz_scanner/flutter_mrz_scanner.dart';
import 'package:permission_handler/permission_handler.dart';

import 'local_widgets/text_item.dart';
import '../cubit/camera_screen_cubit.dart';
import '../model/mrz_model.dart';

class ScanScreen extends StatefulWidget {
  static const routeName = '/ScanScreen';
  const ScanScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _ScanScreenState createState() => _ScanScreenState();
}

class _ScanScreenState extends State<ScanScreen> {
  @override
  void initState() {
    context.read<CameraScreenCubit>().checkPermission();
    super.initState();
  }

  MRZResult? result;
  bool isParsed = false;
  MRZController? controller;
  @override
  void dispose() {
    controller?.stopPreview();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('The Metanest Flutter Test')),
      body: BlocBuilder<CameraScreenCubit, CameraScreenState>(
        builder: (context, state) {
          if (state is CameraScreenReady) {
            return MRZScanner(
                withOverlay: false, onControllerCreated: onControllerCreated);
          } else if (state is CameraScreenFailed) {
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Permission Denied \n You must allow it by yourself from the settings',
                  textAlign: TextAlign.center,
                ),
                OutlinedButton(
                    onPressed: () {
                      openAppSettings();
                    },
                    child: const Text("Allow Permission"))
              ],
            ));
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  void onControllerCreated(MRZController controller) {
    this.controller = controller;

    controller.onParsed = (result) async {
      if (isParsed) {
        return;
      }
      result = result;
      isParsed = true;
      await showModalBottomSheet(
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Column(
                children: [
                  TextItem(title: "Document type", value: result.documentType),
                  TextItem(title: 'Country: ', value: result.countryCode),
                  TextItem(title: 'Surnames: ', value: result.surnames),
                  TextItem(title: 'Given names: ', value: result.givenNames),
                  TextItem(title: 'Document number: ', value: result.documentNumber),
                  TextItem(
                      title: 'Nationality code: ', value: result.nationalityCountryCode),
                  TextItem(title: 'Birthdate: ', value: result.birthDate.toString()),
                  TextItem(title: 'Sex: ', value: result.sex.name),
                  TextItem(title: 'Expriy date: ', value: result.expiryDate.toString()),
                  TextItem(title: 'Personal number: ', value: result.personalNumber),
                  TextItem(
                      title: 'Personal number 2: ',
                      value: result.personalNumber2.toString()),
                  ElevatedButton(
                    child: const Text('Done'),
                    onPressed: () {
                      controller.takePhoto();
                      setState(() {
                        isParsed = false;
                      });
                      return Navigator.pop(context, true);
                    },
                  ),
                ],
              ),
            ),
          );
        },
      ).whenComplete(() {
        setState(() {
          isParsed = false;
        });
      });
    };
    controller.onError = (error) => log(error);

    controller.startPreview();
  }
}
