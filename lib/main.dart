import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mrz_test/app/features/camera_screen/ui/scan_screen.dart';
import 'package:mrz_test/core/extensions/context_shortcuts.dart';
import 'package:mrz_test/core/routes/app_router.dart';

import 'app/features/camera_screen/cubit/camera_screen_cubit.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CameraScreenCubit(),
      lazy: false,
      child: Builder(builder: (context) {
        return const MaterialApp(
          title: 'The Metanest Flutter Test',
          debugShowCheckedModeBanner: false,
          onGenerateRoute: AppRouter.generateRoute,
          home: AppWrapper(),
        );
      }),
    );
  }
}

class AppWrapper extends StatelessWidget {
  const AppWrapper({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("The Metanest Flutter Test"),
        centerTitle: true,
      ),
      body: Center(
        child: OutlinedButton(
            onPressed: () => context.go(ScanScreen.routeName),
            child: const Text("Click To Scan")),
      ),
    );
  }
}

// openAppSettings