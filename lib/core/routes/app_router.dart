import 'package:flutter/material.dart';

import '../../app/features/camera_screen/ui/scan_screen.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings? settings) {
    switch (settings!.name) {
      case ScanScreen.routeName:
        return MaterialPageRoute(builder: (_) => const ScanScreen());
      default:
        return MaterialPageRoute(builder: (_) => const ScanScreen());
    }
  }
}
