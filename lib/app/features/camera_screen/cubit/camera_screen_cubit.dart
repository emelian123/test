import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';

part 'camera_screen_state.dart';

class CameraScreenCubit extends Cubit<CameraScreenState> {
  CameraScreenCubit() : super(CameraScreenInitial());

  Future<void> checkPermission() async {
    var status = await Permission.camera.request();
    switch (status) {
      case PermissionStatus.granted:
        emit(CameraScreenReady());
        break;
      case PermissionStatus.permanentlyDenied:
        emit(CameraScreenFailed(message: "Permission Denied"));
        break;
      case PermissionStatus.denied:
        emit(CameraScreenFailed(message: "Permission Denied"));
        break;
      default:
    }
  }
}
