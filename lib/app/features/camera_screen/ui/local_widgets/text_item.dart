import 'package:flutter/material.dart';

class TextItem extends StatelessWidget {
  final String title;
  final String value;

  const TextItem({
    super.key,
    required this.title,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(title, style: const TextStyle(color: Colors.red)),
        const SizedBox(width: 20),
        Text(value),
      ],
    );
  }
}
