part of 'camera_screen_cubit.dart';

sealed class CameraScreenState {}

final class CameraScreenInitial extends CameraScreenState {}

final class CameraScreenReady extends CameraScreenState {}

final class CameraScreenFailed extends CameraScreenState {
  final String message;
  CameraScreenFailed({required this.message});
}
